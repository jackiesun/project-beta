from django.db import models


# Create your models here.
class Appointment(models.Model):
    vin = models.ForeignKey("VinVo", related_name="appointment", on_delete=models.PROTECT)
    customer_name = models.CharField(max_length=50)
    date_time = models.DateTimeField()
    technician = models.ForeignKey("Technician", related_name="appointment", on_delete=models.PROTECT)
    reason = models.CharField(max_length=50)
    FINISH = "FINISH"
    INPROGRESS = "IN-PROGRESS"
    STATUS_PROGRESS = [
        (FINISH, "FINISH"),
        (INPROGRESS, "IN-PROGRESS")
    ]
    status_progress = models.CharField(max_length=50, choices=STATUS_PROGRESS, default=INPROGRESS)


    def __str__(self):
        return self.customer_name


class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name

class VinVO(models.Model):
    import_href = models.CharField(max_length=50, null=True)
    vin = models.CharField(max_length=17, unique=True)
    VIP = "VIP"
    REG = "Regular"
    VIP_STATUS= [
        (VIP, "VIP"),
        (REG, "Regular")
    ]
    vip_status = models.CharField(max_length=10, choices=VIP_STATUS, default=REG)

    def __str__(self):
        return self.vin
