from django.contrib import admin

# Register your models here.
from .models import Technician, Appointment, VinVO

admin.site.register(VinVO)
admin.site.register(Technician)
admin.site.register(Appointment)
