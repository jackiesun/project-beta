from django.shortcuts import render

from .models import Appointment, Technician, VinVO

from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder

from django.http import JsonResponse

import json
# Create your views here.

class TechnicianForm(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id"
    ]

class VinVOForm(ModelEncoder):
    model = VinVO
    properties = [
        "import_href",
        "vin",
        "vip_status",
    ]

class AppointmentList(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "date_time",
        "technician",
        "reason",
        "id",
        "status_progress"
    ]

    encoders = {
        "vin":VinVOForm()
    }

    def get_extra_data(self, o):
        return {"technician": o.technician.name }

class AppointmentDetail(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",
        "date_time",
        "technician",
        "reason",
        "id"
    ]

    encoders = {
        "technician": TechnicianForm(),
        "vin": VinVOForm(),
    }


class VinVoList(ModelEncoder):
    model = VinVO
    properties = [
        "vin",
        "id"
    ]

@require_http_methods(["GET"])
def api_list_VinVO(request):
    if request.method =="GET":
        vin = VinVO.objects.all()
        return JsonResponse(vin, encoder=VinVoList, safe=False)

@require_http_methods(["GET", "POST"])
def api_form_technician(request):
    if request.method == "POST":
        content = json.loads(request.body)
        print("CONTENT", content)
    else:
        tech = Technician.objects.all()
        return JsonResponse(tech, encoder=TechnicianForm, safe=False)
    tech = Technician.objects.create(**content)
    print("TECH", tech)
    return JsonResponse(tech, encoder=TechnicianForm, safe=False)


@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appoint = Appointment.objects.all()
        print("APPOINT", appoint)
        return JsonResponse(appoint, encoder=AppointmentList, safe=False)
    else:
        content = json.loads(request.body)
        print("CONTENT", content)
        try:
            if VinVO.objects.filter(vin=content["vin"]).exists():
                vin = VinVO.objects.get(vin=content["vin"])
                VinVO.objects.filter(vin=content["vin"]).update(vip_status="VIP")
            else:
                vin = VinVO.objects.create(vin=content["vin"])
            print("VIN", vin)
            content["vin"] = vin
            print(content)
            tech = Technician.objects.get(employee_number=content["technician"])
            content["technician"] = tech
        except VinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vin ID"},
                status=400
            )
    appointment = Appointment.objects.create(**content)
    print("APPOINTMENT", appointment)
    return JsonResponse(appointment, encoder=AppointmentDetail, safe=False)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentDetail, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        Appointment.objects.filter(id=content["id"]).update(status_progress="FINISHED")
        appoint = Appointment.objects.get(id=pk)
        return JsonResponse(appoint, encoder=AppointmentList, safe=False)
    else:
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({ "deleted": count > 0 })
