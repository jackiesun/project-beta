from django.urls import path

from .views import (
    api_form_technician,
    api_list_appointment,
    api_list_VinVO,
    api_show_appointment
)

urlpatterns = [
    path("technician/", api_form_technician, name="tech_form"),
    path("appointment/", api_list_appointment, name="appointment_list"),
    path("appointment/<int:pk>/", api_show_appointment, name="appointment_detail"),
    path("vinVO/", api_list_VinVO, name="vinVO_list"),
]
