import React, { useState } from 'react';

const ManufacturerForm = () => {
    const [name, setName] = useState("");

    const clearManufacturerForm = () => {
        setName("");
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const manufacturerURL = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify({
                name: name
            }),
            headers: {
                "Content-type": "application/json",
            },
        }
        const response = await fetch(manufacturerURL, fetchConfig);
        const data = await response.json();
        if (response.ok) {
            clearManufacturerForm()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form id="create-manufacturer-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="name" required type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm
