import React, {useState, useEffect} from "react"

function AppointmentHistory(app) {
    const [ appoint, setAppointment ] = useState([]);
    const [ searchInput, setSearchInput ] = useState("");


    useEffect(() => {
        const getAppointHistory = async () => {
            const response = await fetch("http://localhost:8080/api/appointment/");
            const data = await response.json();
            if (response.ok){
                setAppointment(data)
                setAppointment(data => data.filter((x) => x.status_progress === "FINISHED"))

            }
        }
        getAppointHistory()
    }, [])


    return(
        <>
        <div>
            <h2>Service appointments History</h2>
            <input type="text" placeholder="Search here" onChange={(event) => setSearchInput(event.target.value)} value={searchInput} label="Search" />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>24 Clock Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appoint.filter(app => {
                        if (searchInput === "") {
                            return app
                        } else if (app.vin.vin.includes(searchInput)) {
                            return app
                        }
                    }).map((app) => {
                        return(
                            <tr key={app.id}>
                                <td>{app.vin.vin}</td>
                                <td>{app.customer_name}</td>
                                <td>{app.date_time.slice(0,10)}</td>
                                <td>{app.date_time.slice(11,16)}</td>
                                <td>{app.technician}</td>
                                <td>{app.reason}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </>
    )
}

export default AppointmentHistory
