import React, {useState, useEffect} from "react"

function AppointmentList() {
    const [ appoint, setAppointment ] = useState([]);

    useEffect(() => {
        const getAppointment = async () => {
            const response = await fetch("http://localhost:8080/api/appointment/");
            const data = await response.json();
            setAppointment(data)
            setAppointment(data => data.filter((x) => x.status_progress !== "FINISHED"))
        }
        getAppointment()
    }, [])

    const deleteAppointment = async (id) => {
        const deleteAppointmentUrl = `http://localhost:8080/api/appointment/${id}/`
        const fetchConfig = {
            method: "delete"
        }
        const response = await fetch(deleteAppointmentUrl, fetchConfig)
        if (response.ok) {
            const confirmation = await response.json()
            setAppointment(prevAppointment => prevAppointment.filter((s) => s.id !==id ))
        }
    }

    const handleFinished = async (id) => {
        const appointFinishedURL = `http://localhost:8080/api/appointment/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({
                id: id
            }),
        }
        const response = await fetch(appointFinishedURL, fetchConfig)
        if (response.ok) {
            const confirmation = await response.json()
            setAppointment(prevAppointment => prevAppointment.filter((s) => s.id !==id ))
        }
    }

    return(
        <>
        <h2>Service appointments</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Status</th>
                    <th>VIN</th>
                    <th>Customer name</th>
                    <th>Date</th>
                    <th>24 Clock Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appoint.map((app) => {
                    return(
                        <tr key={app.id}>
                            <td>{app.vin.vip_status}</td>
                            <td>{app.vin.vin}</td>
                            <td>{app.customer_name}</td>
                            <td>{app.date_time.slice(0,10)}</td>
                            <td>{app.date_time.slice(11,16)}</td>
                            {/* <td> {<TimeFormating app = {app}/>} </td> */}
                            <td>{app.technician}</td>
                            <td>{app.reason}</td>
                            <td>
                                <button onClick={() => deleteAppointment(app.id)} type="button" value="submit" className="deletebtn">Cancel</button>
                            </td>
                            <td>
                                <button onClick={() => handleFinished(app.id)} type="button" value="submit" className="deletebtn">Finished</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default AppointmentList
