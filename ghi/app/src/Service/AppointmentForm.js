import React, { useEffect, useState } from 'react';

const AppointmentForm = () => {
    const [ vin, setVin ] = useState("");
    const [ name, setName ] = useState("");
    const [ time, setTime ] = useState("");
    const [ tech, setTech ] = useState("");
    const [ techs, setTechs ] = useState([]);
    const [ reason, setReason ] = useState("");

    const clearAppointmentForm = () => {
        setVin("");
        setName("");
        setTime("");
        setTech("");
        setReason("");
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const appointURL = "http://localhost:8080/api/appointment/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify({
                vin: vin,
                customer_name: name,
                date_time: time,
                technician: tech,
                reason: reason,
            }),
            headers: {
                "Content-type": "application/json",
            },
        }
        const response = await fetch(appointURL, fetchConfig);
        const data = await response.json();
        if (response.ok) {
            clearAppointmentForm()
        }
    }

    useEffect(() => {
        const getTech = async () => {
            const response = await fetch("http://localhost:8080/api/technician/");
            const data = await response.json();
            setTechs(data)
        }
        getTech()
    }, [])
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Appointment</h1>
                    <form id="create-appointment-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="vin" required type="text" value={vin} onChange={(event) => setVin(event.target.value)} id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">Vin</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="name" required type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="time" required type="datetime-local" name="time" value={time} onChange={(event) => setTime(event.target.value)} id="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="mb-3">
                            <select value={tech} onChange={(event => setTech(event.target.value))} required id="tech" name="tech" className="form-select">
                                <option value="">Choose a tech</option>
                                {techs.map(tech => {
                                    return <option key={tech.employee_number} value={tech.employee_number}>{tech.name}</option>
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="reason" required type="text" name="reason" value={reason} onChange={(event) => setReason(event.target.value)} id="reason" className="form-control" />
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm
