import React, { useState, useEffect } from 'react';

const AutomobileForm = () => {
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [model, setModel] = useState("");
    const [models, setModels] = useState([]);

    const clearAutoForm = () => {
        setColor("");
        setYear("");
        setVin("");
        setModel("");
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const autoURL = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                color: color,
                year: year,
                vin: vin,
                model_id: model
            }),
            headers: {
                "Content-type": "application/json",
            },
        }
        const response = await fetch(autoURL, fetchConfig);
        const data = await response.json();
        if (response.ok) {
            clearAutoForm()
        }
    }

    useEffect(() => {
        const getModel = async () => {
            const response = await fetch("http://localhost:8100/api/models/");
            const data = await response.json();
            setModels(data.models)
        }
        getModel()
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form id="create-auto-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="color" required type="text" value={color} onChange={(event) => setColor(event.target.value)} id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="year" required type="number" name="year" value={year} onChange={(event) => setYear(event.target.value)} id="year" className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="vin" required type="text" name="vin" value={vin} onChange={(event) => setVin(event.target.value)} id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="mb-3">
                            <select value={model} onChange={(event => setModel(event.target.value))} required id="model" name="model" className="form-select">
                                <option value="">Choose a model</option>
                                {models.map(model => {
                                    return <option key={model.id} value={model.id}>{model.name}</option>
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );

}

export default AutomobileForm
