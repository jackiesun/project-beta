import React, {useState, useEffect} from "react"

function AutomobileList() {

    const [ auto, setAuto ] = useState([]);

    useEffect(() => {
        const getAuto = async () => {
            const response = await fetch("http://localhost:8100/api/automobiles/");
            const data = await response.json();
            setAuto(data.autos);
        }
        getAuto()
    }, [])

    return(
        <>
        <h2>Vehicle models</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Vin</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {auto.map((auto) => {
                    return(
                        <tr key={auto.id}>
                            <td>{auto.vin}</td>
                            <td>{auto.color}</td>
                            <td>{auto.year}</td>
                            <td>{auto.model.name}</td>
                            <td>{auto.model.manufacturer.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )
}

export default AutomobileList
