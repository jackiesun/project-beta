import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <li className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturer/list">Manufacturer List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturer/new">Create a Manufacturer</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Models
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/vehicle/list">Vehicle Model list </NavLink></li>
                <li><NavLink className="dropdown-item" to="/vehicle/new">Create a New Vehicle Model</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/auto/list">Inventory List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/auto/new">Add Automobile to Inventory</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales People
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/salespeople/list">Sales People List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/new">Create a New Salesperson</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/customers/list">Customer List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new">Create a New Customer</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales Records
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/records/list">Sales Records</NavLink></li>
                <li><NavLink className="dropdown-item" to="/records/new">Create Sales Record</NavLink></li>
                <li><NavLink className="dropdown-item" to="/records/sellerhistory">Seller History</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointment
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointment/list">Appointment List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointment/new">Create Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointment/history">History Appointment</NavLink></li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technician
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/technician/new">Technician Form</NavLink></li>
              </ul>
            </div>
          </li>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
