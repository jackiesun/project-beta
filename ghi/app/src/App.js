import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AutoForm from './Automobile_Info/AutomobileForm';
import AutomobileList from './Automobile_Info/AutomobileList';
import MainPage from './MainPage';
import ManufacturerForm from './Manufacturer/ManufacturerForm';
import ManufacturerList from './Manufacturer/ManufacturersList';
import Nav from './Nav';
import CustomerForm from './Sales/CustomerForm';
import CustomerList from './Sales/CustomerList';
import SalesPersonForm from './Sales/SalesPersonForm';
import SalesHistoryList from './Sales/SalesPersonHistory';
import SalesPersonList from './Sales/SalesPersonList';
import SalesRecordForm from './Sales/SalesRecordForm';
import SalesRecordList from './Sales/SalesRecordList';
import AppointmentForm from './Service/AppointmentForm';
import AppointmentHistory from './Service/AppointmentHistory';
import AppointmentList from './Service/AppointmentList';
import TechnicianForm from './Service/TechnicianForm';
import VehicleForm from './Vehicle_Model/VehicleForm';
import VehicleList from './Vehicle_Model/VehicleList';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturer">
            <Route path="list" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="vehicle">
            <Route path="list" element={<VehicleList />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>
          <Route path="auto">
            <Route path="list" element={<AutomobileList />} />
            <Route path="new" element={<AutoForm />} />
          </Route>
          <Route path="salespeople">
            <Route path="list" element={<SalesPersonList/>} />
            <Route path="new" element={<SalesPersonForm/>} />
          </Route>
          <Route path="customers">
            <Route path="list" element={<CustomerList/>} />
            <Route path="new" element={<CustomerForm/>} />
          </Route>
          <Route path="records">
            <Route path="list" element={<SalesRecordList/>} />
            <Route path="new" element={<SalesRecordForm/>} />
            <Route path="sellerhistory" element={<SalesHistoryList/>} />
          </Route>
          <Route path="appointment">
            <Route path="list" element={<AppointmentList />} />
            <Route path='new' element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path='technician'>
            <Route path='new' element={<TechnicianForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
