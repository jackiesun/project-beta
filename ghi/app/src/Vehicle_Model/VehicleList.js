import React, {useState, useEffect} from "react"


function VehicleList() {

    const [ vehicle, setVehicle ] = useState([]);

    useEffect(() => {
        const getVehicle = async () => {
            const response = await fetch("http://localhost:8100/api/models/");
            const data = await response.json();
            setVehicle(data.models);
        }
        getVehicle()
    }, []);

    return(
        <>
        <h2>Vehicle models</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {vehicle.map((vehicle) => {
                    return(
                        <tr key={vehicle.id}>
                            <td>{vehicle.name}</td>
                            <td>{vehicle.manufacturer.name}</td>
                            <td><img src={vehicle.picture_url} alt="" /></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    )

}

export default VehicleList
