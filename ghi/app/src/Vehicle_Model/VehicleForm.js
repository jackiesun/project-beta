import React, { useEffect, useState } from 'react';

const VehicleForm = () => {
  const [name, setName] = useState("");
  const [picture_url, setPicture] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([])

  const clearVehicleForm = () => {
    setName("");
    setPicture("");
    setManufacturer("");
  }

  const handleSubmit = async (submit) => {
    submit.preventDefault()
    const vehicleURL = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        name: name,
        picture_url: picture_url,
        manufacturer_id: manufacturer
      }),
      headers: {
        "Content-type": "application/json",
      },
    }
    const response = await fetch(vehicleURL, fetchConfig);
    const data = await response.json();
    if (response.ok) {
      clearVehicleForm()
    }
  }

  useEffect(() => {
    const getManufacturer = async () => {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
    getManufacturer()
  }, [])


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form id="create-vehicle-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input placeholder="name" required type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} id="name" className="form-control" />
              <label htmlFor="name">Model name</label>
            </div>
            <div className="form-floating mb-3">
              <input placeholder="picture_url" required type="url" name="picture_url" value={picture_url} onChange={(event) => setPicture(event.target.value)} id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture</label>
            </div>
            <div className="mb-3">
              <select value={manufacturer} onChange={(event => setManufacturer(event.target.value))} required id="manufacturer" name="manufacturer" className="form-select">
                <option value="">Choose a manufacturer</option>
                {manufacturers.map(manufacturer => {
                  return <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}

export default VehicleForm
