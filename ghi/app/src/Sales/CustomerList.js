import React, { useState, useEffect } from "react"


function CustomerList() {

    const [customer, setCustomer] = useState([]);

    useEffect(() => {
        const getCustomer = async () => {
            const response = await fetch("http://localhost:8090/api/customers/")
            const data = await response.json()
            setCustomer(data.potential_customers)
        }
        getCustomer()
    }, []);

    return (
        <>
            <h2>Potential Customers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map((customer) => {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}


export default CustomerList
