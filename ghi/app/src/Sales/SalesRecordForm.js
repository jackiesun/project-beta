import React, { useEffect, useState } from 'react'

const SalesRecordForm = () => {
    const [automobile, setAutomobile] = useState("")
    const [automobiles, setAutomobiles] = useState([])
    const [sales_person, setSalesPerson] = useState("")
    const [sales_persons, setSalesPersons] = useState([])
    const [customer, setCustomer] = useState("")
    const [customers, setCustomers] = useState([])
    const [price, setPrice] = useState("")

    const clearSalesRecordForm = () => {
        setAutomobile("")
        setSalesPerson("")
        setCustomer("")
        setPrice("")
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const salesRecordURL = "http://localhost:8090/api/sales/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                automobile: automobile,
                sales_person: sales_person,
                customer: customer,
                price: price
            }),
            headers: {
                "Content-type": "application/json"
            }
        }


        const response = await fetch(salesRecordURL, fetchConfig)
        const data = await response.json()
        if (response.ok) {
            clearSalesRecordForm()
            window.location.reload(false)
        }
    }

    useEffect(() => {
        const getAutomobiles = async () => {
            const response = await fetch("http://localhost:8090/api/autos/")
            const data = await response.json()
            setAutomobiles(data)
            setAutomobiles(data => data.filter((x) => x.status === "UNSOLD"))
        }
        getAutomobiles()
    }, [])

    useEffect(() => {
        const getSalesPersons = async () => {
            const response = await fetch("http://localhost:8090/api/salespeople/")
            const data = await response.json()
            setSalesPersons(data.sales_people)
        }
        getSalesPersons()
    }, [])

    useEffect(() => {
        const getCustomers = async () => {
            const response = await fetch("http://localhost:8090/api/customers/")
            const data = await response.json()
            setCustomers(data.potential_customers)
        }
        getCustomers()
    }, [])


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form id="create-record-form" onSubmit={handleSubmit}>

                        <div className="mb-3">
                            <select value={automobile} onChange={(event => setAutomobile(event.target.value))} required id="automobile" name="automobile" className="form-select">
                                <option value="">Choose an automobile</option>
                                {automobiles.map(automobile => {
                                    return <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={sales_person} onChange={(event => setSalesPerson(event.target.value))} required id="sales_person" name="sales_person" className="form-select">
                                <option value="">Choose a sales person</option>
                                {sales_persons.map(salesperson => {
                                    return <option key={salesperson.id} value={salesperson.employee_number}>{salesperson.name}</option>
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={customer} onChange={(event => setCustomer(event.target.value))} required id="customer" name="customer" className="form-select">
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return <option key={customer.id} value={customer.phone}>{customer.name}</option>
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="price" required type="number" name="price" value={price} onChange={(event) => setPrice(event.target.value)} id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalesRecordForm
