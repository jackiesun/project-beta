import React, { useState, useEffect } from 'react'

function SalesHistoryList() {

    const [salesHistory, setSalesHistory] = useState([])
    const [seller, setSeller] = useState("")
    const [sellers, setSellers] = useState([])




    useEffect(() => {
        const getHistory = async (value) => {
            const url = (`http://localhost:8090/api/sales/${value}`)
            const fetchConfig = {
                method: "get",
                headers: {
                    "Content-type": "application/json"
                }
            }
            const response = await fetch(url, fetchConfig)
            const data = await response.json()
            setSalesHistory(data.sales)

        }
        getHistory(seller)
    }, [seller])

    useEffect(() => {
        const getSellers = async () => {
            const response = await fetch("http://localhost:8090/api/salespeople/")
            const data = await response.json()
            setSellers(data.sales_people)
        }
        getSellers()
    }, [])

    return (
        <>
            <h2>Sales person history</h2>
            <div className="mb-3">
                <select value={seller} onChange={(event) => setSeller(event.target.value)} required id="seller" name="seller" className="form-select">
                    <option value="">Choose an seller</option>
                    {sellers.map(seller => {
                        return <option key={seller.id} value={seller.id}>{seller.name}</option>
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {salesHistory?.map((sales) => {
                        return (
                            <tr key={sales.id}>
                                <td>{sales.sales_person.name}</td>
                                <td>{sales.customer.name}</td>
                                <td>{sales.automobile.vin}</td>
                                <td>{sales.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )


}

export default SalesHistoryList
