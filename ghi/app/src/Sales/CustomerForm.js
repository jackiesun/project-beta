import React, { useState } from 'react'

const CustomerForm = () => {
    const [name, setName] = useState("")
    const [address, setAddress] = useState("")
    const [phone, setPhone] = useState("")

    const clearCustomerForm = () => {
        setName("")
        setAddress("")
        setPhone("")
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const customerURL = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name: name,
                address: address,
                phone: phone
            }),
            headers: {
                "Content-type": "application/json"
            }
        }
        const response = await fetch(customerURL, fetchConfig)
        const data = await response.json()
        if (response.ok) {
            clearCustomerForm()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Potential Customer</h1>
                    <form id="create-customer-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="name" required type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="address" required type="text" name="address" value={address} onChange={(event) => setAddress(event.target.value)} id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="phone" required type="number" name="phone" value={phone} onChange={(event) => setPhone(event.target.value)} id="phone" className="form-control" />
                            <label htmlFor="phone">Phone</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )







}

export default CustomerForm
