import React, { useState, useEffect } from 'react'

function SalesRecordList() {

    const [salesRecord, setSalesRecord] = useState([])

    useEffect(() => {
        const getSalesRecord = async () => {
            const response = await fetch("http://localhost:8090/api/sales/")
            const data = await response.json()
            setSalesRecord(data.sales_records)
        }
        getSalesRecord()
    }, [])

    return (
        <>
            <h2>Sales Records</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person Name</th>
                        <th>Employee Number</th>
                        <th>Purchaser Name</th>
                        <th>Automobile VIN</th>
                        <th>Price of Sale</th>
                    </tr>
                </thead>
                <tbody>
                    {salesRecord.map((salesRecord) => {
                        return (
                            <tr key={salesRecord.id}>
                                <td>{salesRecord.sales_person.name}</td>
                                <td>{salesRecord.sales_person.employee_number}</td>
                                <td>{salesRecord.customer.name}</td>
                                <td>{salesRecord.automobile.vin}</td>
                                <td>${salesRecord.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )


}

export default SalesRecordList
