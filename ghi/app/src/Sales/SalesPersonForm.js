import React, { useState } from 'react'

const SalesPersonForm = () => {
    const [name, setName] = useState("")
    const [employee_number, setEmployeeNumber] = useState("")

    const clearSalesPersonForm = () => {
        setName("")
        setEmployeeNumber("")
    }

    const handleSubmit = async (submit) => {
        submit.preventDefault()
        const salespersonURL = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name: name,
                employee_number: employee_number
            }),
            headers: {
                "Content-type": "application/json"
            }
        }
        const response = await fetch(salespersonURL, fetchConfig)
        const data = await response.json()
        if (response.ok) {
            clearSalesPersonForm()
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Sales Person</h1>
                    <form id="create-salesperson-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="name" required type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="employee_number" required type="number" name="employee_number" value={employee_number} onChange={(event) => setEmployeeNumber(event.target.value)} id="employee_number" className="form-control" />
                            <label htmlFor="employee_number">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )







}

export default SalesPersonForm
