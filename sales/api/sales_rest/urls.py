from django.urls import path

from .views import (
    api_sales_people,
    api_potential_customers,
    api_sales_records,
    api_show_seller_sales,
    api_automobile_vos
)

urlpatterns = [
    path("salespeople/", api_sales_people, name="api_create_sales_person"),
    path("customers/", api_potential_customers, name="api_create_customers"),
    path("sales/", api_sales_records, name="api_create_sales_record"),
    path("sales/<int:pk>/", api_show_seller_sales, name="api_show_seller_sales"),
    path("autos/", api_automobile_vos, name="api_list_autos"),

]
